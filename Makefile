ifneq ("$(wildcard /usr/share/sharness/sharness.sh)","")
SHARNESS := /usr/share/sharness
endif

ifneq ("$(wildcard /usr/local/share/sharness/sharness.sh)","")
SHARNESS := /usr/local/share/sharness
endif

ifneq ("$(wildcard $HOME/.local/share/sharness/sharness.sh)","")
SHARNESS := $HOME/.local/share/sharness
endif

PREFIX ?= /usr/local
CFLAGS ?= -march=native -O3 -fomit-frame-pointer

txt2unix: target/release/txt2unix-rs
	cp $< $@

target/release/txt2unix-rs: FORCE
	cargo build --release

target/debug/txt2unix-rs: FORCE
	cargo build

txt2unix.1: MANPAGE.md
	ronn < $< > $@

test/auto.t: test/txt2unix.py
	python3 $< -t > $@

test: txt2unix test/manual.t test/auto.t
	SHARNESS_TEST_SRCDIR=$(SHARNESS) TXT2UNIX=$(CURDIR)/txt2unix prove -v test/manual.t
	SHARNESS_TEST_SRCDIR=$(SHARNESS) TXT2UNIX=$(CURDIR)/txt2unix prove -v test/auto.t

prefix ?= $(PREFIX)
bindir ?= $(prefix)/bin
mandir ?= $(prefix)/share/man/man1
install: txt2unix txt2unix.1
	mkdir -p $(DESTDIR)$(bindir)
	mkdir -p $(DESTDIR)$(mandir)
	install -m 755 txt2unix $(DESTDIR)$(bindir)/
	install -m 644 txt2unix.1 $(DESTDIR)$(mandir)/

clean:
	cargo clean
	rm -f txt2unix txt2unix.1

FORCE:

.PHONY: clean test install FORCE
