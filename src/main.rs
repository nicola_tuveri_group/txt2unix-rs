//! Normalize text file line endings to UNIX-style
//!
//! Inspired by <https://gitlab.com/nisec/txt2unix> by Billy Bob Brumley,
//! reimplemented in 🦀 Rust by Nicola Tuveri, for fun and for easier
//! handling of Unicode.

#![deny(missing_docs)]

use clap::Parser;
use encoding_rs::Encoding;
use encoding_rs_io::{DecodeReaderBytesBuilder, DecodeReaderBytes};
use std::io::{self, Read, Write};
use unicode_reader::Graphemes;
use chardet_rs_bin;

/// Normalize text file line endings to UNIX-style
///
/// The utility `txt2unix` reads from standard input (_stdin_) and writes to
/// standard output (_stdout_).
/// It copies the input to the output, but replaces carriage returns with line
/// feeds.
/// Except in the carriage return + line feed case, which it replaces with a
/// single line feed.
///
/// This helps to normalize the line endings in text files to UNIX line endings.
///
/// I hesitate to call it "converting" because that might imply a priori
/// knowledge about the input line endings.
/// In contrast to utilities like `dos2unix`, `unix2dos`, `mac2unix`,
/// `unix2mac`, `d2u`, `u2d`, or various `tr`/`perl`/`sed` (often
/// incorrect/non-portable) oneliners you might find on StackOverflow,
/// `txt2unix` makes no assumptions about the input.
/// This makes `txt2unix` particularly well-suited for inputs with unknown or
/// possibly mixed line endings.
#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct CLIArgs {
    /// Ensure output ends in a line feed character
    #[clap(short = 'n')]
    force_end_with_newline: bool,

    /// Suppress NUL characters encountered in input
    #[clap(short = 'z')]
    suppress_nul: bool,

    /// Specify the source encoding.
    ///
    /// *NOTE*: Output encoding is always UTF-8.
    ///
    /// Check <https://encoding.spec.whatwg.org/#concept-encoding-get>
    /// for a list of valid labels.
    ///
    /// - The special value "BOM" tries to sniff for a BOM marker, or defaults
    ///   to UTF-8 otherwise.
    ///
    /// - The special value "auto" tries to guess the input encoding, via
    ///   `chardetng` heuristics.
    #[clap(short, long, default_value = "BOM")]
    encoding: String,
}

fn main() -> io::Result<()> {
    let args = CLIArgs::parse();

    // lock stdin and stdout to speed up via buffers
    let stdout = io::stdout().lock();
    let mut stdin = io::stdin().lock();
    let mut buffer: Vec<u8> = Vec::new();

    let decoder: DecodeReaderBytes<Box<dyn Read>, Vec<u8>> = match args.encoding.as_str() {
        "BOM" => DecodeReaderBytesBuilder::new()
            .utf8_passthru(true)
            .bom_sniffing(true)
            .bom_override(true)
            .strip_bom(true)
            .build(Box::new(stdin)),
        "auto" => {
            stdin.read_to_end(&mut buffer)?;
            let encoding = chardet_rs_bin::chardet(&buffer);
            // FIXME: what happens if the above fails?

            DecodeReaderBytesBuilder::new()
                .utf8_passthru(true)
                .encoding(Some(encoding))
                .bom_sniffing(false)
                .bom_override(false)
                .strip_bom(true)
                .build(Box::new(&buffer[..]))
        },
        other => {
            let encoding = Encoding::for_label_no_replacement(other.as_bytes())
                .ok_or_else(|| {
                    panic!(
                        "The encoding label provided ('{}') is invalid.",
                        other
                    );
                })
                .unwrap();

            DecodeReaderBytesBuilder::new()
                .utf8_passthru(true)
                .encoding(Some(encoding))
                .strip_bom(true)
                .build(Box::new(stdin))
        }
    };

    let opts: Options = args.into();

    filter(decoder, stdout, opts)
}

struct Options {
    force_end_with_newline: bool,
    suppress_nul: bool,
}

impl From<CLIArgs> for Options {
    fn from(args: CLIArgs) -> Self {
        Options {
            force_end_with_newline: args.force_end_with_newline,
            suppress_nul: args.suppress_nul,
        }
    }
}

/// The <Null> character
const NUL: &str = "\x00";

/// The Unicode "Replacement Character": � (U+FFFD)
const REPLACEMENT: &str = "\u{FFFD}";

/// Carriage Return
const CR: &str = "\r";

/// Line Feed
const LF: &str = "\n";

/// Carriage Return + Line Feed
const CRLF: &str = "\r\n";

/// Defines the output line ending to enforce.
const OEOL: &str = LF;

/// Iterates over `r`, one _grapheme_ at a time, replacing line endings and
/// emitting to `w`.
/// Both `r` and `w` should be buffered for performance.
fn filter(r: impl Read, mut w: impl Write, opts: Options) -> io::Result<()> {
    // construct our input iterator
    let mut graphemes = Graphemes::from(r);
    let mut last_was_eol = false;
    let mut gcnt = 0u64;
    let mut nulcnt = 0u64;
    let mut rplcmnt_cnt = 0u64;

    while let Some(g) = graphemes.next() {
        // unwrap the input grapheme
        let input = g?;
        gcnt += 1;

        // do something with the input
        let output;
        let emit: bool;
        (output, last_was_eol, emit) = match input.as_str() {
            NUL => {
                if opts.suppress_nul {
                    nulcnt += 1;
                    eprintln!(
                        "G{}: NUL character suppressed (cnt={})",
                        gcnt, nulcnt
                    );
                    (NUL, false, false)
                } else {
                    (NUL, false, true)
                }
            }
            REPLACEMENT => {
                rplcmnt_cnt += 1;
                eprintln!("G{}: met a � character (cnt={})", gcnt, rplcmnt_cnt);
                (REPLACEMENT, false, true)
            }
            LF | CR | CRLF => (OEOL, true, true),
            other => (other, false, true),
        };

        if emit {
            // emit to writer
            w.write_all(output.as_bytes())?;
        }
    }

    if opts.force_end_with_newline && !last_was_eol {
        w.write_all(OEOL.as_bytes())?;
    }

    // flush buffers
    w.flush()?;

    // Done!
    Ok(())
}
