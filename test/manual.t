#!/bin/bash

test_description="manually specified tests"

: "${SHARNESS_TEST_SRCDIR:=.}"
. "$SHARNESS_TEST_SRCDIR/sharness.sh"

# negative tests: bad options
test_expect_success "txt2unix -a: bad option negative test" "test_must_fail $TXT2UNIX -a < ../corpus/nix.txt"
test_expect_success "txt2unix -b: bad option negative test" "test_must_fail $TXT2UNIX -b < ../corpus/win.txt"
test_expect_success "txt2unix -c: bad option negative test" "test_must_fail $TXT2UNIX -c < ../corpus/mac.txt"

# positive tests: manual stuff
test_expect_success "txt2unix < corpus/nix.txt" "$TXT2UNIX < ../corpus/nix.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix < corpus/win.txt" "$TXT2UNIX < ../corpus/win.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix < corpus/mac.txt" "$TXT2UNIX < ../corpus/mac.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix -n < corpus/nix.txt" "$TXT2UNIX -n < ../corpus/nix.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix -n < corpus/win.txt" "$TXT2UNIX -n < ../corpus/win.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix -n < corpus/mac.txt" "$TXT2UNIX -n < ../corpus/mac.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix < corpus/nix_nonewline.txt" "$TXT2UNIX < ../corpus/nix_nonewline.txt | md5sum | grep ^35231757333a1c2bb56302dae76e2d28"
test_expect_success "txt2unix < corpus/win_nonewline.txt" "$TXT2UNIX < ../corpus/win_nonewline.txt | md5sum | grep ^35231757333a1c2bb56302dae76e2d28"
test_expect_success "txt2unix < corpus/mac_nonewline.txt" "$TXT2UNIX < ../corpus/mac_nonewline.txt | md5sum | grep ^35231757333a1c2bb56302dae76e2d28"
test_expect_success "txt2unix -n < corpus/nix_nonewline.txt" "$TXT2UNIX -n < ../corpus/nix_nonewline.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix -n < corpus/win_nonewline.txt" "$TXT2UNIX -n < ../corpus/win_nonewline.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix -n < corpus/mac_nonewline.txt" "$TXT2UNIX -n < ../corpus/mac_nonewline.txt | md5sum | grep ^cbd618d1fab40120e0b5631354af50ca"
test_expect_success "txt2unix < corpus/empty.txt" "$TXT2UNIX < ../corpus/empty.txt | md5sum | grep ^d41d8cd98f00b204e9800998ecf8427e"
test_expect_success "txt2unix -n < corpus/empty.txt" "$TXT2UNIX -n < ../corpus/empty.txt | md5sum | grep ^68b329da9893e34099c7d8ad5cb9c940"
test_expect_success "txt2unix < corpus/nix_oneemptline.txt" "$TXT2UNIX < ../corpus/nix_oneemptyline.txt | md5sum | grep ^68b329da9893e34099c7d8ad5cb9c940"
test_expect_success "txt2unix < corpus/win_oneemptyline.txt" "$TXT2UNIX < ../corpus/win_oneemptyline.txt | md5sum | grep ^68b329da9893e34099c7d8ad5cb9c940"
test_expect_success "txt2unix < corpus/mac_oneemptyline.txt" "$TXT2UNIX < ../corpus/mac_oneemptyline.txt | md5sum | grep ^68b329da9893e34099c7d8ad5cb9c940"

test_done
