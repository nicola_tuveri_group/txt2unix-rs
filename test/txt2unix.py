#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2022 Billy Bob Brumley, D.Sc. (Tech.)
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
# AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import sys
import getopt
import random
import hashlib

lipsum = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam metus mi,
cursus non gravida ut, gravida et ligula. Aenean id metus fermentum,
porttitor ipsum at, semper leo. Duis eleifend, tellus sit amet mattis
bibendum, libero magna faucibus enim, sed ullamcorper nunc ante in
nisi. Nam sem ex, congue quis consequat id, efficitur vitae nunc. Nulla
fermentum, ante nec posuere blandit, purus nibh porttitor tellus, eget
tincidunt lectus risus nec augue. Nulla sagittis porta hendrerit. Donec
quis erat in massa elementum semper vehicula eget dolor. Aliquam facilisis
turpis a lectus finibus, vestibulum feugiat neque facilisis. Duis at
scelerisque mauris. Aliquam non cursus mauris. Nulla semper lacinia arcu
quis lacinia. Aenean gravida mi et libero lobortis, in facilisis lorem
placerat. Morbi id tempor nulla.

Praesent fringilla mi vulputate lacus egestas, at pharetra eros
congue. Praesent gravida libero quam, nec sodales dui tincidunt nec. In
rutrum enim eget aliquet bibendum. Aliquam sollicitudin felis neque,
nec ultricies neque lobortis eget. Ut consequat feugiat odio, eget
euismod dui rhoncus ultricies. Nunc libero nunc, posuere at ornare quis,
euismod quis lacus. Nunc fringilla mi lorem, non commodo nibh pulvinar
quis. Suspendisse lobortis rutrum feugiat. Quisque sed elementum justo,
non auctor urna. Vivamus scelerisque urna arcu, at tincidunt purus
placerat vel. Sed varius eget leo a facilisis.

Morbi est massa, viverra facilisis libero nec, sollicitudin sodales
quam. Donec sed vestibulum purus, non vestibulum erat. Mauris consequat
libero orci, et efficitur neque feugiat vel. Ut rutrum magna quis lacus
eleifend, vel pulvinar enim tempus. Etiam fermentum augue luctus varius
tempus. Duis lorem mi, ullamcorper ut ultricies quis, ullamcorper
ut nunc. Curabitur nec dui erat. In a sem viverra, commodo orci nec,
rhoncus sapien.

Mauris bibendum ante vel purus tempus vehicula. Fusce eu risus vel leo
aliquam tempor a et neque. Aenean egestas pellentesque quam. Vestibulum
placerat rutrum ipsum.
"""

def normalize(s, trailing=False):
    s = s.replace('\r\n', '\n')
    s = s.replace('\r', '\n')
    if trailing and (not s or s[-1] != '\n'): s += '\n'
    return s

def test_generate():
    random.seed('053c14e180767f5be71b340c8b862d45')
    words = lipsum.replace('\n', ' ')
    words = words.replace('.', '')
    words = words.replace(',', '')
    while '  ' in words: words = words.replace('  ', ' ')
    words = words.strip()
    words = words.split(' ')

    # header
    print(r"""#!/bin/bash

test_description="automated tests"

: "${SHARNESS_TEST_SRCDIR:=.}"
. "$SHARNESS_TEST_SRCDIR/sharness.sh"
""")

    for i in range(1, 101):
        l = ['\n'] * i + ['\r\n'] * i + ['\r'] * i + words
        random.shuffle(l)
        s = ' '.join(l)
        s = s.replace(' \n', '\n')
        s = s.replace(' \r', '\r')
        s = s.replace('\n ', '\n')
        s = s.replace('\r ', '\r')
        print('test_expect_success "txt2unix automated test #%d: sin -n" "printf %s | $TXT2UNIX    | md5sum | grep ^%s"' % (i, repr(s), hashlib.md5(normalize(s).encode('ascii')).hexdigest()))
        print('test_expect_success "txt2unix automated test #%d: con -n" "printf %s | $TXT2UNIX -n | md5sum | grep ^%s"' % (i, repr(s), hashlib.md5(normalize(s, trailing=True).encode('ascii')).hexdigest()))

    # footer
    print("\ntest_done")

if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "nt")
    except getopt.GetoptError as err:
        print(err)
        sys.exit(1)

    trailing = False
    for o, a in opts:
        if o == "-n":
            trailing = True
        elif o == "-t":
            test_generate()
            sys.exit(0)
        else:
            assert False, "unhandled option"

    s = sys.stdin.read()
    print(normalize(s, trailing), end='')
