# `txt2unix-rs` — Normalize text file line endings to UNIX-style

Inspired by <https://gitlab.com/nisec/txt2unix> by Billy Bob Brumley,
reimplemented in 🦀 Rust by Nicola Tuveri, for fun and for easier
handling of Unicode.

Read the full [original manpage for `txt2unix`](./MANPAGE.md).